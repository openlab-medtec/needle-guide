# Source README

## ToDo

- Presspassung auslegen (Temperatur für Kleinteile berechnen)
- FreeCAD assembly (download Assembly3 from Addon Manager)
- Zeichnung anlegen

## Press Fit

`stop` and `funnel` are to be mounted onto `pipe` with a press fit.
Ultimately, the only force the press fit has to withstand is translational (while handling the guide).
It is assumed that $65 N$ would give a sufficiently solid fit.

`stop` and `funnel` are to be turned, which gives an average roughness value $Ra = 0.2 ... 25 \mu m$.


